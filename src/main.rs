use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder};
use project2_ci_rust_microservice::greedy_coin_change;
use serde::Serialize;

// Define a struct to serialize the response
#[derive(Serialize)]
struct ChangeResponse {
    dollars: u32,
    cents: u32,
    change: Vec<u32>,
}

#[get("/")]
// Handler for the root route
async fn root() -> impl Responder {
    "
    Greedy Coin Change Machine

    **Primary Route:**
    /change/{dollars}/{cents}
    "
}

#[post("/echo")]
async fn echo(req_body: String) -> impl Responder {
    HttpResponse::Ok().body(req_body)
}

#[get("/change/{dollars}/{cents}")]
// Handler for the change calculation
async fn change(info: web::Path<(u32, u32)>) -> HttpResponse {
    let (dollars, cents) = info.into_inner();
    // Convert to cents
    let amount = dollars * 100 + cents;
    let change = greedy_coin_change(amount);
    let response = ChangeResponse {
        dollars,
        cents,
        change,
    };
    HttpResponse::Ok().json(response) // Respond with JSON
}

async fn manual_hello() -> impl Responder {
    HttpResponse::Ok().body("Hey there!")
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(root)
            .service(echo)
            .service(change)
            .route("/hey", web::get().to(manual_hello))
    })
    .bind(("0.0.0.0", 8080))?
    .run()
    .await
}
