# project2_CI_rust_microservice

## Demo Video
[Watch the video](https://drive.google.com/file/d/1fq8SNSY9dRKKiZKCsZhDJwa-cuz18SPz/view?usp=drive_link)

## Initial Setup
1. create a new repository on gitlab and clone it to this local machine
2. `cargo init` to create the basic structure of the project
3. add the following dependencies to the `Cargo.toml` file
```toml
[dependencies]
actix-web = "4"
```

## Create the simple business logic for the web app
1. edit the `main.rs` file according to [Actix Getting Started](https://actix.rs/docs/getting-started) to create a simple web server
2. `cargo run` to start the web server
3. open a web browser and go to `http://localhost:8080/` to see the web server in action
![alt text](images/test.png)
4. `Ctrl+C` to stop the web server


## Upgrade the business logic for the web app
1. create a new file `lib.rs` in the `src` directory
2. add the greedy coin algorithm to the `lib.rs` file
3. using `cargo test` to test until the greedy coin algorithm perform correctly
![alt text](images/cargo_test.png)
4. edit the `main.rs` file to use the greedy coin algorithm
5. `cargo run` to start the web server and test the greedy coin algorithm
	- <img src="images/business_logic.png" alt="drawing" style="width:200px;"/>


## Containerize the web app with Distroless image
1. create a new file `Dockerfile` in the root directory
2. build the rust project with the following command
	`cargo build --release`
2. add the following content to the `Dockerfile`
```Dockerfile
# Build stage
FROM rust:1.68 AS build
WORKDIR /app
COPY . .
RUN cargo build --release

# Production stage
FROM gcr.io/distroless/cc-debian11
COPY --from=build /app/target/release/project2_ci_rust_microservice /app/

# use non-root user
USER nonroot:nonroot

# Set up App directory
ENV APP_HOME=/app
WORKDIR $APP_HOME

# Expose the port
EXPOSE 8080

# Run the web service on container startup.
ENTRYPOINT [ "/app/project2_ci_rust_microservice" ]
```
3. build the docker image with the following command
> It will take a while to build the image

- ![alt text](images/image_build_process.png)

- ![alt text](images/actix_webapp_image_created.png)
4. run the docker container with the following command (Make sure the server port of the source code is 0.0.0.0:8080 not 127.0.0.1:8080)
```bash
docker run -p 8080:8080 individual_2
```
- ![alt text](images/container_start.png)

5. open a web browser and go to `http://localhost:8080/` to see the web server in action.
- <img src="images/docker_run.png" alt="drawing" style="width:200px;"/> 
- <img src="images/docker_run2.png" alt="drawing" style="width:200px;"/>


6. `docker ps` to see the running container
7. `docker stop` and `docker rm` to stop and remove the running container

## CI/CD with Gitlab CI/CD
1. create a new file `.gitlab-ci.yml` in the root directory
2. add the following content to the `.gitlab-ci.yml` file
```yaml
image: "rust:latest"

stages:
  - build
  - test
  - dockerize

build:
  stage: build
  image: rust:latest
  script:
    - cargo build --verbose

test-code:
  stage: test
  script:
    - cargo test
    - cargo install cargo-tarpaulin
    - cargo tarpaulin --ignore-tests

lint-code:
  stage: test
  script:
    - rustup component add rustfmt
    - cargo fmt -- --check
    - rustup component add clippy
    - cargo clippy -- -D warnings

audit-code:
  stage: test
  script:
    - cargo install cargo-audit
    - cargo audit


dockerize:
  stage: dockerize
  image: docker:latest
  before_script:
  - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  services:
    - docker:dind
  script:
    - docker --version
    - docker build -t individual_2 .
    - docker run -d -p 8080:8080 individual_2
    - docker ps -a
```
3. push the code to the gitlab repository, the dockerize stage is aiming to build the docker image and run the container
